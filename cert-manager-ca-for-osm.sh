#create CA cert and key
brew install step
step certificate create root.osm.cluster.local ca.crt ca.key \
--not-after=87600h --profile root-ca --no-password --insecure

kubectl create ns cert-manager
kubectl create secret generic osm-ca-bundle -n cert-manager --from-file=tls.key=ca.key --from-file=tls.crt=ca.crt

helm upgrade -i cert-manager \
  --namespace cert-manager --create-namespace \
  --set installCRDs=true \
  --set ingressShim.defaultIssuerName=osm-ca \
  --set ingressShim.defaultIssuerKind=ClusterIssuer \
  jetstack/cert-manager

kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: osm-ca
spec:
  ca:
    secretName: osm-ca-bundle
EOF

kubectl create ns osm-system
kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: osm-ca
  namespace: osm-system
spec:
  isCA: true
  duration: 87600h # 365 days
  secretName: osm-ca-bundle
  commonName: osm-system
  issuerRef:
    name: osm-ca
    kind: ClusterIssuer
    group: cert-manager.io
EOF

helm upgrade -i -n osm-system --create-namespace \
    --set=osm.enablePermissiveTrafficPolicy=true \
    --set=osm.deployPrometheus=true \
    --set=osm.deployGrafana=true \
    --set=osm.deployJaeger=true \
    --set contour.enabled=true \
    --set osm.deployJaeger=true \
    --set osm.featureFlags.enableMulticlusterMode=true \
    --set osm.meshName=master \
    --set osm.certificateProvider.kind=cert-manager \
    --set osm.certmanager.issuerKind=ClusterIssuer \
    osm osm/osm